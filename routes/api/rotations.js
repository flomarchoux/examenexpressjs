const router = require('express').Router();
const {Rotation: Rotations} = require('../../db/mongoose');

router
    .route('/rotations')
    .get((req, res, next) => {
        Rotations.find().then((data) => {
            res.json(data);
        })
            .catch((e)=>{next(e)});
    })
    .post((req, res, next) => {
        const rotation = new Rotations({
            maxNewPlayerLevel: '10',
            freeChampionIdsForNewPlayers: '20',
            freeChampionIds: '2',
        });

        rotation.save().then(
            (data) => {
                res.status(200);
                res.json(data);
            }
        )

    })
;

module.exports = router;
